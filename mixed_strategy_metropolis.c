#include <stdio.h>
#include <math.h>

#define coupling_A -1
#define coupling_B 0

void mixed_strategy_metropolis(
  int size_int_,
  int mcs_int_,
  int strategy_int_ary_[ size_int_ ],
  double beta_double_,
  gsl_rng * rng_ptr
  ){

  int new_configuration_int = 0 ;
  int choosen_site_int = 0;
  int metropolis_trial_int = 1 ;

  double delta_energy_double = 0.0 ;


  choosen_site_int = (int)(mcs_int_* gsl_rng_uniform_int( rng_ptr, size_int_ ))%size_int_ ;
  
  new_configuration_int = (-1)*( strategy_int_ary_[choosen_site_int] ) ;

  if ( choosen_site_int == size_int_-1 )
  {
    delta_energy_double = \
      coupling_A*((new_configuration_int - strategy_int_ary_[ choosen_site_int ])*strategy_int_ary_[0])+\
      coupling_B*(new_configuration_int-strategy_int_ary_[ choosen_site_int ]) ;
  }
  else{
    delta_energy_double = \
      coupling_A*((new_configuration_int - strategy_int_ary_[choosen_site_int])*strategy_int_ary_[choosen_site_int+1])+\
      coupling_B*(new_configuration_int-strategy_int_ary_[choosen_site_int]) ;
  }

  if( delta_energy_double > 0.0 ){

     metropolis_trial_int = gsl_rng_uniform( rng_ptr ) < exp( -beta_double_*delta_energy_double ) ? 1 : 0 ;

  }
  // printf("%.2f\t%.2f\n",second_random_double_ary_[mcs_int_] ,exp(-beta_double_*delta_energy_double) );
  // printf("%d\n",metropolis_trial_int );
  strategy_int_ary_[choosen_site_int] = (metropolis_trial_int*new_configuration_int)+\
    (1- metropolis_trial_int)*strategy_int_ary_[choosen_site_int] ;

}