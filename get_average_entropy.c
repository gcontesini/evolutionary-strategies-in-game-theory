#include <stdio.h>
#include <math.h>

double get_average_entropy(
    int ensemble_probability_size_int_,
    double strategies_profile_double_ary_[ ensemble_probability_size_int_ ]
  ){
  
  int index_int = 0 ;

  double ensemble_entropy_double = 0.0 ;

  for (
    index_int = 0 ;
    index_int < ensemble_probability_size_int_ ;
    index_int++
  ){
    ensemble_entropy_double = ensemble_entropy_double-\
    (
      strategies_profile_double_ary_[index_int]*log(1.e-6+strategies_profile_double_ary_[index_int])
    ) ;
  }

  return ensemble_entropy_double ;
}