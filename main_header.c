#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_rng.h>

#include "init_random_strategy.c"
#include "init_all_coop_strategy.c"
#include "init_all_desertion_strategy.c"

#include "set_ensemble_probability.c"

#include "get_global_strategy.c"
#include "get_global_energy.c"

#include "get_average_entropy.c"
#include "get_average_energy.c"

#include "mixed_strategy_metropolis.c"