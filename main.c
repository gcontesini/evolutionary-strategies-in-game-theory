#include "main_header.c"

int main(
	int argc,
	char *argv[]
){

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                     PROFILE
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/*
	argv:
		N_size_int_ = 128
		ensemble_size_int = 10
		mcs_int_ = 1e6
		beta_double = 5.0
		first_seed_int_ = 123456
		second_seed_int_ = 123456
		first seed is responsible for the strategy initialization.
		second seed is responsible for the simulation dynamics, Metropolis.

	*/
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                   VARIABLES
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	int ensemble_size_int = 10 ;
	int first_seed_int = 123456 ;
	int mcs_int = 1e6 ;

	int index_mcs_int = 0 ;
	
	long int n_size_int = 128 ;
	long int ensemble_probability_size_int = 0 ;

	double global_average_energy_double = 0.0 ;
	double global_average_strategy_double = 0.0 ;
	double ensemble_average_entropy_double = 0.0 ;
	double ensemble_average_energy_double = 0.0 ;

	double beta_double = 0.0 ;
	double delta_beta_double = 1 ;

	int *strategies_int_ary_ptr = NULL ; 
	
	double *strategies_profile_double_ary_ptr = NULL ;


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                        ARGV
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if(argc>1){
		n_size_int = atoi(argv[1]) ;
	}

	if(argc>2){
		ensemble_size_int = atoi(argv[2]) ;
	}

	if(argc>3){
		mcs_int = atoi(argv[3]) ;
	}

	if(argc>4){
		mcs_int = atoi(argv[4]) ;
	}

	if(argc>5){
		first_seed_int = atoi(argv[5]) ;
	}


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                 OUTPUT FILE
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	FILE * data_file_ptr = NULL ;

  char* begin_str = "./Data/data_file_N-"; 
  char* btw_str = "_E-"; 
  char* ext_str = ".txt";
  char data_file_str[ strlen(begin_str) + strlen(ext_str) + strlen(btw_str) + 7 ];

  snprintf(
  	data_file_str,
  	sizeof( data_file_str ),
  	"%s%ld%s%d%s",
  	begin_str,
  	n_size_int,
  	btw_str,
  	ensemble_size_int,
  	ext_str
  );

  data_file_ptr = fopen (
  	data_file_str,
  	"w"
  ) ;

	if(
		data_file_ptr == NULL
	){
	  printf("FILE ERROR!") ;   
	  return 1 ;             
	}

	// fprintf(
	// 	data_file_ptr,
	// 	"Simulation_Time\tEnsemble_size\tEntropy\tEnergy\tTemperature"
	// ) ;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                  SEED TABLE
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	gsl_rng * rng_ptr = gsl_rng_alloc( gsl_rng_ranlxs2 ) ;

  gsl_rng_set( rng_ptr, first_seed_int ) ;

	// for (int i = 0; i < n_size_int; ++i)
	// {
	// 	printf("%d\t%lf\t%lf\n",i, first_random_double_ary_ptr[i], second_random_double_ary_ptr[i] );
	// }

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                              INITIALIZATION
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



	ensemble_probability_size_int = pow( 2, ensemble_size_int ) ;

	strategies_int_ary_ptr = malloc( n_size_int * sizeof(double) ) ;
	
	strategies_profile_double_ary_ptr = malloc(
		ensemble_probability_size_int*sizeof(double)
	);
	
	init_random_strategy(
		n_size_int,
		strategies_int_ary_ptr,
		rng_ptr
	) ;

	// init_all_coop_strategy(
	// 	n_size_int,
	// 	strategies_int_ary_ptr
	// ) ;


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                                  SIMULATION
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	while(
		beta_double <= 5.0
	){

		for (
			index_mcs_int = 0;
			index_mcs_int < mcs_int;
			index_mcs_int++
		){

			// printf("%.2f\t%.2f\t%.2f\t",beta_double, global_average_energy_double,global_average_strategy_double );

			global_average_strategy_double = get_global_strategy(
				n_size_int,
				strategies_int_ary_ptr
			) ;

			global_average_energy_double = get_global_energy(
				n_size_int,
				strategies_int_ary_ptr
			) ;

			set_ensemble_probability(
				n_size_int,
				strategies_int_ary_ptr,
				ensemble_size_int,
				ensemble_probability_size_int,
				strategies_profile_double_ary_ptr
			);

			// ensemble_average_energy_double = get_average_energy(
			// 	ensemble_size_int,
			// 	ensemble_probability_size_int,
			// 	strategies_profile_double_ary_ptr
			// ) ;

			// ensemble_average_entropy_double = get_average_entropy(
			// 	ensemble_probability_size_int,
			// 	strategies_profile_double_ary_ptr
			// ) ;

			
			// for (int i = 0; i < n_size_int; ++i)
			// {
			// 	fprintf(data_file_ptr,"%d ",strategies_int_ary_ptr[ i ] );
			// }

			mixed_strategy_metropolis(
				n_size_int,
				index_mcs_int,
				strategies_int_ary_ptr,
				beta_double,
				rng_ptr
			);

			if(index_mcs_int%15000==0){		

				printf(
					"%.2f\t%d\t%.4lf\t%.4lf\t%.4f\t%.4f\n",
					beta_double,
					index_mcs_int,
					global_average_energy_double,
					global_average_strategy_double,
					ensemble_average_entropy_double,
					ensemble_average_energy_double
				);
		
			}

			// 	fprintf(
			// 		data_file_ptr,
			// 		"%.2f\t%d\t%.4lf\t%.4lf\t%.4f\t%.4f\n",
			// 		beta_double,
			// 		index_mcs_int,
			// 		global_average_energy_double,
			// 		global_average_strategy_double,
			// 		ensemble_average_entropy_double,
			// 		ensemble_average_energy_double
			// 	);

		}

		beta_double = beta_double + delta_beta_double ;
	
	}
		// printf("\n");
		// n_size_int = n_size_int*2 ;
	// }

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//                                                         MEMORY DEALLOCATION
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  free( strategies_int_ary_ptr ) ;
  free( strategies_profile_double_ary_ptr ) ; 

	gsl_rng_free( rng_ptr ) ;

  fclose ( data_file_ptr ) ;

  return 0;
}

