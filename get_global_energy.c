
#define coupling_A -1
#define coupling_B 0

double get_global_energy(
  int size_int_,
  int strategies_int_ary_[ size_int_ ]
  ){

  int energy_index_int = 0 ;

  double total_energy_double = 0 ;

  total_energy_double = total_energy_double +\
    coupling_A*( strategies_int_ary_[ 0 ]*strategies_int_ary_[ 1 ] )+\
    coupling_A*( strategies_int_ary_[ 0 ]*strategies_int_ary_[ size_int_-1 ] )+\
    coupling_B*( strategies_int_ary_[ 0 ] ) ;

  for (
    energy_index_int = 1 ;
    energy_index_int < size_int_-1 ;
    energy_index_int++
  ){

    total_energy_double = total_energy_double +\
      coupling_A*( strategies_int_ary_[ energy_index_int ]*strategies_int_ary_[ energy_index_int+1 ] )+\
      coupling_A*( strategies_int_ary_[ energy_index_int ]*strategies_int_ary_[ energy_index_int-1 ] )+\
      coupling_B*( strategies_int_ary_[ energy_index_int ] ) ;

    }

  total_energy_double = total_energy_double +\
    coupling_A*( strategies_int_ary_[ energy_index_int+1 ]*strategies_int_ary_[ 0 ] )+\
    coupling_A*( strategies_int_ary_[ energy_index_int+1 ]*strategies_int_ary_[ energy_index_int ] )+\
    coupling_B*( strategies_int_ary_[ energy_index_int+1 ] ) ;
 
  return total_energy_double/size_int_ ;

}