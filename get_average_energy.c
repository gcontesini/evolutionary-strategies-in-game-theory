#include <stdlib.h>

#include "get_label_configuration.c"

#define coupling_A -1
#define coupling_B 0

double get_average_energy(
  	int ensemble_size_int_,
  	int ensemble_probability_size_int_,
  	double strategies_profile_double_ary_[ ensemble_probability_size_int_ ]
	){

	int energy_index_int = 0 ;
  int label_index_int = 0 ;
  int number_configurations_int = pow(2, ensemble_size_int_) ;
  double configuration_energy_double = 0.0 ;
  double average_energy_double = 0.0 ;

  for (
    label_index_int = 0 ;
    label_index_int < number_configurations_int ;
    label_index_int++
  ){
    /* code */
    // average_energy_double = 0.0 ;
    configuration_energy_double = 0.0 ;
    int *configuration_int_ary = NULL ;

    configuration_int_ary = malloc( ensemble_size_int_ * sizeof(int) ) ;

    get_label_configuration( label_index_int, ensemble_size_int_, configuration_int_ary ) ;

    for (
      energy_index_int = 0 ;
      energy_index_int < ensemble_size_int_-1 ;
      energy_index_int++
    ){

    	configuration_energy_double = configuration_energy_double +\
      coupling_A*( configuration_int_ary[ energy_index_int ]*configuration_int_ary[ energy_index_int+1 ] )+\
      coupling_B*( configuration_int_ary[ energy_index_int ] ) ;

    }
    configuration_energy_double = configuration_energy_double +\
      coupling_A*( configuration_int_ary[ energy_index_int+1 ]*configuration_int_ary[ 0 ] )+\
      coupling_B*( configuration_int_ary[ energy_index_int+1 ] ) ;

    average_energy_double = average_energy_double + \
    ( configuration_energy_double*strategies_profile_double_ary_[ label_index_int ] ) ;
    
    free( configuration_int_ary ) ;
  }

  
  return average_energy_double/ensemble_size_int_ ;

}