#include <math.h>

void set_ensemble_probability(
	int size_int_,
	int strategy_int_ary_[ size_int_ ],
	int ensemble_size_int_,
	int probability_profile_size_int_,
	double ensemble_probability_profile_ary_[ probability_profile_size_int_ ]
	){

	int ary_index_int = 0 ;
	int ensemble_index_int = 0 ;
	int micro_state_label_int = 0 ;

	double sum_double = 0.0 ;
	
  double ensemble_count_profile_double_ary[ probability_profile_size_int_ ] ;

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Array-Counter Cleaner 
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for (
		ary_index_int = 0 ;
		ary_index_int <= probability_profile_size_int_;
		ary_index_int++
	){

		ensemble_count_profile_double_ary[ ary_index_int ] = 0.0 ;
		ensemble_probability_profile_ary_[ ary_index_int ] = 0.0 ;
		// printf("%d:%f\n",ary_index_int,ensemble_count_profile_double_ary[ary_index_int] );
	}
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Count which and how many micro states there are.
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	for (
		ary_index_int = 0 ;
		ary_index_int <= size_int_- ensemble_size_int_ ;
		ary_index_int++
	){
		
		micro_state_label_int = 0 ;

		for (
			ensemble_index_int = ensemble_size_int_ - 1 ;
			ensemble_index_int >= 0  ;
			ensemble_index_int--
		){
			
			micro_state_label_int =	\
				micro_state_label_int + (
					((1+strategy_int_ary_[ ary_index_int+ensemble_index_int ])/2)*\
					pow( 2, ensemble_index_int ) 
				) ;
				// printf("%d,",strategy_int_ary_[ary_index_int+ensemble_index_int] );
				
		}
		// printf("%d\n",micro_state_label_int);
		ensemble_count_profile_double_ary[ micro_state_label_int ]++ ;

		// printf("%d:%.2lf\n", micro_state_label_int,ensemble_count_profile_double_ary[micro_state_label_int] ); 
		
	}
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Normalization of Probability Distribution .
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for (
		ary_index_int = 0 ;
		ary_index_int <= probability_profile_size_int_ ;
		ary_index_int++
	){

		sum_double = sum_double + ensemble_count_profile_double_ary[ary_index_int] ;
		// printf("%d,%lf\n",ary_index_int, sum_double );
	}

	for (
		ary_index_int = 0 ;
		ary_index_int < probability_profile_size_int_ ;
		ary_index_int++
	){

		ensemble_probability_profile_ary_[ ary_index_int ] = \
			ensemble_count_profile_double_ary[ ary_index_int ] / sum_double ; 
		// printf("%d\t%f\n", ary_index_int, ensemble_probability_profile_ary_[ary_index_int] );
	}

}

// ainda falta a condição de contorno !