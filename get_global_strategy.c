double get_global_strategy(
	int size_int_,
	int strategy_int_ary_[ size_int_ ]
	){

	int index_int = 0 ;
	double sum_double = 0.0 ;

	sum_double = 0.0 ;

	for (
		index_int = 0 ;
		index_int < size_int_ ;
		index_int++
	){
		sum_double = sum_double + strategy_int_ary_[ index_int ] ;
	}

	return sum_double/size_int_ ;
}