#include <stdio.h>

void get_label_configuration(
  int label_int_,
  int ensemble_size_int_,
  int configuration_ary_[ ensemble_size_int_ ]
){

  int index_int = 0 ;
  int aux_label_int = label_int_ ;

  for (
    index_int = 0 ;
    index_int < ensemble_size_int_ ;
    index_int++
  ){
    configuration_ary_[ index_int ] = 2*(aux_label_int%2)-1 ;
    aux_label_int = aux_label_int/2 ;
  }

}