void init_random_strategy(
	int size_int_,
	int strategy_int_ary_[size_int_],
	gsl_rng * rng_gsl_ptr_
	){

	int index_int = 0 ;

	for( index_int=0; index_int<size_int_; index_int++ ){
		
		strategy_int_ary_[index_int] = -1;

		if( gsl_rng_uniform( rng_gsl_ptr_ ) < 0.5 ){ //Trocar pelo gsl

			strategy_int_ary_[index_int] = 1;
		
		}	
	}
}